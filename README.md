This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions to run the project

Assuming you have node and npm installed go to project root and run

### `npm install`

This should install you the node modules and project dependencies

### `npm start`

Launches the test runner in the interactive watch mode.<br />
