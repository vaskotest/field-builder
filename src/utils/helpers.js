import { DEFAULT_STATE } from "./config";
export const getInitialState = () => {
  const jsonState = window.localStorage.getItem("appState");
  if (jsonState) {
    return JSON.parse(jsonState);
  }

  return DEFAULT_STATE;
};
