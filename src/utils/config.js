export const DEFAULT_STATE = {
  label: "",
  required: false,
  defaultValue: "",
  order: "asc",
  orderChoices: [
    { key: "asc", value: "Ascending" },
    { key: "desc", value: "Descending" },
  ],
  multipleSelectChoices: [],
  newChoiceValue: "",
};

export const MAX_CHOICE_NUMBER = 4;
