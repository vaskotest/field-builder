import React from "react";

const SubmitButton = (props) => {
  return (
    <button type="button" className="btn btn-success" onClick={props.onClick}>
      {props.loading ? (
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        >
          Loading...
        </span>
      ) : (
        props.text
      )}
    </button>
  );
};

export default SubmitButton;
