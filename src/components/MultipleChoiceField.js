import React from "react";
import axios from "axios";
import "../bootstrap/bootstrap.min.css";
import SubmitButton from "../components/SubmitButton";
import { DEFAULT_STATE, MAX_CHOICE_NUMBER } from "../utils/config";
import { getInitialState } from "../utils/helpers";

class MultipleChoiceField extends React.Component {
  constructor(props) {
    super(props);
    const initialState = getInitialState();
    this.state = {
      ...initialState,
      labelValid: true,
      posting: false,
    };
    this.onLabelChange = this.onLabelChange.bind(this);
    this.onDefaultValueChange = this.onDefaultValueChange.bind(this);
    this.onRequiredChange = this.onRequiredChange.bind(this);
    this.onRemoveChoiceClick = this.onRemoveChoiceClick.bind(this);
    this.onAddChoiceClick = this.onAddChoiceClick.bind(this);
    this.onNewChoiceValueChange = this.onNewChoiceValueChange.bind(this);
    this.onCancelClick = this.onCancelClick.bind(this);
    this.onOrderChange = this.onOrderChange.bind(this);
    this.onSaveClick = this.onSaveClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener("beforeunload", (ev) => {
      ev.preventDefault();
      const {
        label,
        required,
        defaultValue,
        order,
        orderChoices,
        multipleSelectChoices,
        newChoiceValue,
      } = this.state;
      const appState = {
        label: label,
        required: required,
        defaultValue: defaultValue,
        order: order,
        orderChoices: orderChoices,
        multipleSelectChoices: multipleSelectChoices,
        newChoiceValue: newChoiceValue,
      };

      window.localStorage.setItem("appState", JSON.stringify(appState));
      return;
    });
  }
  onLabelChange(ev) {
    this.setState({ label: ev.target.value });
  }

  onNewChoiceValueChange(ev) {
    this.setState({ newChoiceValue: ev.target.value });
  }

  onDefaultValueChange(ev) {
    this.setState({ defaultValue: ev.target.value });
  }

  onRequiredChange(ev) {
    this.setState({ required: ev.target.checked });
  }

  onRemoveChoiceClick(ev, choice) {
    this.setState(function (prevState) {
      let choices = prevState.multipleSelectChoices;
      choices = choices.filter((ch) => ch !== choice);
      return { multipleSelectChoices: choices };
    });
  }

  onAddChoiceClick() {
    this.setState(function (prevState) {
      const newChoice = prevState.newChoiceValue;

      if (!newChoice) {
        return;
      }

      let choices = [...prevState.multipleSelectChoices];

      if (choices.length >= MAX_CHOICE_NUMBER) {
        return;
      }

      if (!choices.includes(newChoice)) {
        choices.push(newChoice);
        return { multipleSelectChoices: choices };
      }
    });
  }

  onCancelClick() {
    this.setState({ ...DEFAULT_STATE });
  }

  onOrderChange(ev) {
    const order = ev.target.value;
    this.setState({ order });
  }

  onSaveClick() {
    // TO DO handle the case when value not in choices but
    // choices has reached their limit

    if (!this.state.label) {
      this.setState({ labelValid: false });
      return;
    }

    this.setState(function (prevState) {
      const defaultValue = prevState.defaultValue;
      let choices = prevState.multipleSelectChoices;

      if (defaultValue && !choices.includes(defaultValue)) {
        choices.push(defaultValue);
        return { multipleSelectChoices: choices };
      }
    });

    //TODO maybe when a value is required we also
    //should make the default value required

    this.postData();
  }

  async postData() {
    const url = "http://www.mocky.io/v2/566061f21200008e3aabd919";
    this.setState({ posting: true });

    const payload = {
      label: this.state.label,
      required: this.state.required,
      choices: this.state.multipleSelectChoices,
      displayAlpha: true,
      default: this.state.defaultValue,
      order: this.state.order,
    };

    try {
      const response = await axios.post(url, payload);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }

    this.setState({ posting: false });
  }

  render() {
    const lablelClassName = this.state.labelValid
      ? "form-control"
      : "form-control is-invalid";

    return (
      <div className="col-lg-6 bd-example">
        <div className="card border-info mb-3">
          <div className="card-header bg-info text-white">Field Builder</div>
          <form className="p-4">
            <div className="form-group row">
              <label htmlFor="label" className="col-sm-3 col-form-label">
                Label
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  className={lablelClassName}
                  id="label"
                  value={this.state.label}
                  onChange={this.onLabelChange}
                />
                <div className="invalid-feedback">
                  Please provide a value for the label.
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-3 col-form-label">Type</label>
              <div className="col-sm-9">
                <span htmlFor="label" className="col-sm-1 col-form-label">
                  Multi-select
                </span>
                <div className="form-group form-check-inline">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="multiSelectCheckbox"
                    checked={this.state.required}
                    onChange={this.onRequiredChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="multiSelectCheckbox"
                  >
                    A value is required
                  </label>
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="defaultValue" className="col-sm-3 col-form-label">
                Default Value
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  className="form-control"
                  id="defaultValue"
                  value={this.state.defaultValue}
                  onChange={this.onDefaultValueChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="order" className="col-sm-3 col-form-label">
                Order
              </label>
              <div className="col-sm-9">
                <select
                  id="order"
                  className="form-control"
                  value={this.state.order}
                  onChange={this.onOrderChange}
                >
                  {this.state.orderChoices.map((choice) => (
                    <option key={choice.key} value={choice.key}>
                      {choice.value}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="newChoiceValue"
                className="col-sm-3 col-form-label"
              >
                New Choice
              </label>
              <div className="col-sm-6">
                <input
                  type="text"
                  className="form-control mb-2"
                  id="newChoiceValue"
                  value={this.state.newChoiceValue}
                  onChange={this.onNewChoiceValueChange}
                />
              </div>
              <div className="col-sm-3">
                <button
                  onClick={this.onAddChoiceClick}
                  type="button"
                  className="btn btn-success"
                >
                  Add
                </button>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-3 col-form-label">Choices</label>
              <div className="col-sm-9">
                <ul className="list-group">
                  {this.state.multipleSelectChoices.map((choice) => (
                    <li className="list-group-item" key={choice}>
                      {choice}
                      <button
                        type="button"
                        className="ml-2 mb-1 close"
                        onClick={(ev) => this.onRemoveChoiceClick(ev, choice)}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="form-group row">
              <div className="col-sm-9 offset-sm-3">
                <SubmitButton
                  text={"Save"}
                  onClick={this.onSaveClick}
                  loading={this.state.posting}
                />
                <button
                  type="button"
                  className="btn btn-link text-danger d-inline-block"
                  onClick={this.onCancelClick}
                >
                  Cancel
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default MultipleChoiceField;
